# Kubernetes

## What is Kubernetes?
-	Kubernetes (also known as k8s or “kube”) is an open source container orchestration platform that automates many of the manual processes involved in deploying, managing, and scaling containerized applications. 
-	Kubernetes allows you to manage and coordinate clusters of containers across multiple hosts to ensure efficient resource utilization, high availability, and easy scalability.


## Why we use kubernetes?
Because of the features listed below, we choose Kubernetes:- 
-	Auto Scaling - Kubernetes can automatically scale containerized applications up or down based on demand.
-	Load Balancer - Kubernetes can load balance traffic across multiple containerized applications.
-	Auto Healing - Those containers which are failed during the execution process, Kubernetes restarts them 
    automatically. And, those containers which do not reply to the user-defined health check, it stops them from working automatically.
-	Networking & routing - Kubernetes can provide networking services for containerized applications, such as   
    DNS, IP address allocation, and load balancing.
-	Deployment strategy - Kubernetes can be used to deploy containerized applications to a variety of 
    environments, including on-premises, in the cloud, or in a hybrid environment.
-	Storage: Kubernetes can provide storage services for containerized applications, such as persistent volumes 
    and filesystems.
 
 
## Kubernetes Architecture?

 Kubernetes Architecture.jpg

The architecture of Kubernetes actually follows the client-server architecture. It consists of the following two main components:

### Control Plane/ Master Node
-	The master node hosts the Kubernetes control plane and manages the cluster, including scheduling and        scaling applications and maintaining the state of the cluster.
-	The master node is a critical component of a Kubernetes cluster. If the master node goes down, the entire cluster will be unavailable.
-	For this reason, it is important to have a high availability (HA) configuration for the master node. This can be done by running multiple master nodes in a cluster and using a load balancer to distribute traffic between them.

### Master Node has four different components
#### API Server
-	The Kubernetes API server receives the REST commands which are sent by the user.
-	After receiving, it validates the REST requests, process, and then executes them. After the execution of REST commands, the resulting state of a cluster is saved in 'etcd' as a distributed key-value store.

#### Scheduler
-	The scheduler in a master node schedules the tasks to the worker nodes.

#### Controller Manager
-	The controllers in a master node perform a task and manage the state of the cluster.
-	It is responsible for running different types of controllers that maintain the overall actual state and desired state.

#### ETCD
-	It is an open-source, simple, distributed key-value storage which is used to store the cluster data.
-	It is a part of a master node which is written in a GO programming language.

### Worker Node/ Slave Node
-	A worker node is a node in a Kubernetes cluster that is responsible for running pods.
-	Pods are the basic unit of deployment in Kubernetes. They are groups of containers that are scheduled together on a single node.

### Worker Node has Three different components
#### Kubelet
-	Execute jobs provided by scheduler.
-	Kubelet is a crucial component in Kubernetes that runs on each node within a cluster. 
-	It is responsible for managing and monitoring the state of Pods, ensuring that they are running and healthy according to the desired state defined in the cluster configuration.
-	Every kubelet in each worker node communicates with the master node. It also starts, stops, and maintains the containers which are organized into pods directly by the master node.

#### Kube-proxy
-	Kube-proxy is a network proxy and load balancer that runs on each node in a Kubernetes cluster. 
-	It is responsible for facilitating network communication to and from services and Pods within the cluster.
-	It is a proxy service of Kubernetes, which is executed simply on each worker node in the cluster. The main aim of this component is request forwarding.
-	Each node interacts with the Kubernetes services through Kube-proxy (communication between nodes)

#### Container Engine
-	A container engine, also known as a container runtime, is a software component responsible for running and managing containers on a host machine. 
-	It provides the underlying infrastructure and runtime environment necessary to create, start, stop, and manage containers.
-	The container engine takes care of several important tasks such as Container Lifecycle Management, Resource Isolation, Networking, Storage, Security.

#### Pods
-	In Kubernetes, a Pod is the smallest and most basic unit of deployment. 
-	It is a logical group of one or more containers that are scheduled together on the same node and share the same network namespace. 
-	Pods are used to encapsulate and deploy one or more tightly coupled application components, such as microservices, within a Kubernetes cluster.

## NOTE :-
- In  docker , Docker manages Container
- In Kubernetes, Kubernetes manages PODs
- [[Containerd is default container engine.]]


## Pod Lifecycle

- Kubectl sends command for POD creation    TO    API
- API sends request  TO  ETCD (to check which node is available, on which node want to execute command)
- ETCD passes   TO   API
- API send request    TO    Scheduler
- Scheduler (pass command to schedule the task)   TO    Kubelet
- Kubelet (to run the POD)    TO   Container Engine
- Container Engine (will create POD)     TO   POD (means containers are created)

- NOTE :- All communication will be done through API.


## Objects:
- Pod - Smallest unit of kubernetes. Its just wrapping around the container
- Service - Expose application 
    - ClusterIP - Expose application inside the cluster
    - NodePort - Expose application outside the cluster
    - LoadBalancer - Expose application outside the cluster 
- Namespace -  To segregate the resources / k8s objects